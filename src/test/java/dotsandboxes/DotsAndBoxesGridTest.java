package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static fields.
     * This field is a logger. Loggers are like a more advanced println, for writing messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.git checkoutgetLogger(DotsAndBoxesGridTest.class);

    /*
     * Tests are functions that have an @Test annotation before them.
     * The typical format of a test is that it contains some code that does something, and then one
     * or more assertions to check that a condition holds.
     *
     * This is a dummy test just to show that the test suite itself runs
     */
    @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }

    @Test
    public void testScoreIncreasesWhenBoxCompleted() {
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid();
        // Simulate moves that complete a box
        grid.makeMove(0, 0);
        grid.makeMove(0, 1);
        grid.makeMove(1, 0);
        grid.makeMove(1, 1); // This move completes a box

        // Check if the score has increased appropriately
        assertEquals(1, grid.getScore());
    }

    @Test
    public void testInvalidMoveNotAllowed() {
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid();
        // Make a move
        grid.makeMove(0, 0);
        // Try to make the same move again, which should be invalid
        boolean moveResult = grid.makeMove(0, 0);

        // Check if the invalid move was rejected
        assertFalse(moveResult);
    }

}
